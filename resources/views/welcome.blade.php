<!DOCTYPE html>
<html>
<head>

<!-- Basic -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">   

<title>Sia&nbsp;|&nbsp;Academico</title>  

<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="Porto - Responsive HTML5 Template">
<meta name="author" content="okler.net">

<!-- Favicon -->
<link rel="shortcut icon" href="images/logo_sia.png" type="image/x-icon" />
<link rel="apple-touch-icon" href="images/logo_sia.png">

<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

<!-- Web Fonts  -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

<!-- Vendor CSS -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="vendor/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="vendor/animate/animate.min.css">
<link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
<link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

<!-- Theme CSS -->
<link rel="stylesheet" href="css/theme.css">
<link rel="stylesheet" href="css/theme-elements.css">
<link rel="stylesheet" href="css/theme-blog.css">
<link rel="stylesheet" href="css/theme-shop.css">

<!-- Current Page CSS -->
<link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
<link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
<link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">
<link rel="stylesheet" href="vendor/circle-flip-slideshow/css/component.css">

<!-- Demo CSS -->


<!-- Skin CSS -->
<link rel="stylesheet" href="css/skins/default.css"> 

<!-- Theme Custom CSS -->
<link rel="stylesheet" href="css/custom.css">

<!-- Head Libs -->
<script src="vendor/modernizr/modernizr.min.js"></script>

</head>
<body>

<div class="body">
<header id="header" class="header-transparent header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
<div class="header-body border-top-0 header-body-bottom-border" style="background-color: rgba(255,255,255,0.7)!important;">
<div class="header-container container">
<div class="header-row">
<div class="header-column">
    <div class="header-row">
        <div class="header-logo">
            <a href="index.html">
                <img alt="Porto" width="100" height="68" data-sticky-width="82" data-sticky-height="50" src="images/logo_sia.png">
            </a>
        </div>
    </div>
</div>
<style type="text/css">
/*.dropdown > a{
 border-right: 1px solid #000000;
}
.dropdown:last-child > a{
 border-right: 1px solid #000000;
}*/
</style>
<div class="header-column justify-content-end">
    <div class="header-row">
        <div class="header-nav header-nav-links order-2 order-lg-1">
            <div class="header-nav-main header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
                <nav class="collapse">
                    <ul class="menustyle nav nav-pills" id="mainNav" style="margin-bottom:-2%!important">
                        <li class="dropdown">
                            <a class="dropdown-item dropdown-toggle" href="#" style="font-size:large;word-spacing: 2px;">
                               INICIO
                            </a>
                        </li>
                         <li class="dropdown">
                            <a class="dropdown-item dropdown-toggle" href="#" style="font-size:large;">
                               |
                            </a>
                        </li>
                        <li class="dropdown dropdown-mega">
                            <a class="dropdown-item dropdown-toggle" href="#" style="font-size:large;">
                                BENEFICIOS
                            </a>
                        </li>
                        <li class="divider-vertical-second-menu">
                            <a class="dropdown-item dropdown-toggle" href="#" style="font-size:large;">
                               |
                            </a>
                        </li>
                            <li class="dropdown dropdown-mega">
                            <a class="dropdown-item dropdown-toggle" href="#" style="font-size:large;">
                                CARACTERISTICAS
                            </a>
                        </li>
                        <li class="divider-vertical-second-menu">
                            <a class="dropdown-item dropdown-toggle" href="#" style="font-size:large;">
                               |
                            </a>
                        </li>
                            <li class="dropdown dropdown-mega">
                            <a class="dropdown-item dropdown-toggle" href="#" style="font-size:large;">
                                CLIENTES
                            </a>
                        </li>
                        <li class="divider-vertical-second-menu">
                            <a class="dropdown-item dropdown-toggle" href="#" style="font-size:large;">
                               |
                            </a>
                        </li>
                        <li class="divider-vertical-second-menu"></li>
                        <li class="dropdown">
                            <a class="dropdown-item dropdown-toggle active" href="#" style="font-size:large;">
                                PERFILES
                            </a>
                        </li>
                        <li class="divider-vertical-second-menu">
                            <a class="dropdown-item dropdown-toggle" href="#" style="font-size:large;">
                               |
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-item dropdown-toggle" href="#" style="font-size:large;">
                               FAQ's
                            </a>
                        </li>
                        <li class="divider-vertical-second-menu">
                            <a class="dropdown-item dropdown-toggle" href="#" style="font-size:large;">
                               |
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-item dropdown-toggle" href="#" style="font-size:large;">
                                CONTACTO
                            </a>
                        </li>
                        <li class="divider-vertical-second-menu">
                            <a class="dropdown-item dropdown-toggle" href="#" style="font-size:large;">
                               |
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-item dropdown-toggle" href="#">
                               <img src="images/whatsapp.png" style="max-width:30px">
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</header>

<div role="main" class="main">
<div class="slider-container rev_slider_wrapper bg-color-grey-scale-1" style="height: 100vh;">
<div id="revolutionSlider" class="slider rev_slider" data-version="5.4.8" data-plugin-revolution-slider data-plugin-options="{'sliderLayout': 'fullscreen', 'delay': 9000, 'gridwidth': 1140, 'gridheight': 800, 'responsiveLevels': [4096,1200,992,500]}">
<ul>
<li data-transition="fade">
    <img src="images/inicio1.jpg"  
        alt=""
        data-bgposition="100% 100%" 
        data-bgfit="100% 100%" 
        data-bgrepeat="no-repeat" 
        class="rev-slidebg">                              
</li>
</ul>
</div>
</div>


<div class="container">
<div class="container pt-5 d-flex justify-content-center">
<h2 class="font-weight-normal line-height-1"><strong class="font-weight-extra-bold" style="font-size:200%">PERFILES</strong></h2>
</div>
</div>
<div class="container d-flex justify-content-between">
            <div class="card mr-3 shadow">
                <img class="card-img-top" src="images/icono_redes1.png" alt="Card Image">
                <div class="card-body">
                    <h4 class="card-title mb-1 text-4 font-weight-bold">Card Title</h4>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur rhoncus nulla dui, in dapi.</p>
                    <a href="" class="read-more text-color-primary font-weight-semibold text-2">Read More <i class="fas fa-angle-right position-relative top-1 ml-1"></i></a>
                </div>
            </div>

            <div class="card mr-3 shadow">
                <img class="card-img-top" src="img/blog/wide/blog-11.jpg" alt="Card Image">
                <div class="card-body">
                    <h4 class="card-title mb-1 text-4 font-weight-bold">Card Title</h4>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur rhoncus nulla dui, in dapi.</p>
                    <a href="" class="read-more text-color-primary font-weight-semibold text-2">Read More <i class="fas fa-angle-right position-relative top-1 ml-1"></i></a>
                </div>
            </div>

            <div class="card mr-3 shadow">
                <img class="card-img-top" src="img/blog/wide/blog-11.jpg" alt="Card Image">
                <div class="card-body">
                    <h4 class="card-title mb-1 text-4 font-weight-bold">Card Title</h4>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur rhoncus nulla dui, in dapi.</p>
                    <a href="" class="read-more text-color-primary font-weight-semibold text-2">Read More <i class="fas fa-angle-right position-relative top-1 ml-1"></i></a>
                </div>
            </div>

            <div class="card mr-3 shadow">
                <img class="card-img-top" src="img/blog/wide/blog-11.jpg" alt="Card Image">
                <div class="card-body">
                    <h4 class="card-title mb-1 text-4 font-weight-bold">Card Title</h4>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur rhoncus nulla dui, in dapi.</p>
                    <a href="" class="read-more text-color-primary font-weight-semibold text-2">Read More <i class="fas fa-angle-right position-relative top-1 ml-1"></i></a>
                </div>
            </div>

            <div class="card mr-3 shadow">
                <img class="card-img-top" src="img/blog/wide/blog-11.jpg" alt="Card Image">
                <div class="card-body">
                    <h4 class="card-title mb-1 text-4 font-weight-bold">Card Title</h4>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur rhoncus nulla dui, in dapi.</p>
                    <a href="" class="read-more text-color-primary font-weight-semibold text-2">Read More <i class="fas fa-angle-right position-relative top-1 ml-1"></i></a>
                </div>
            </div>
        </div>


<div class="container py-4">

    <div class="row py-4 mb-4">
        <div class="col">
            <div class="image-hotspots">
                <img src="images/iphone_promedio.png" class="img-fluid">
            </div>
        </div>
    </div>

</div>


<div class="container pt-5 d-flex justify-content-center">
<h2 class="font-weight-normal line-height-1"><strong class="font-weight-extra-bold" style="font-size:200%;margin-left: auto;margin-right: auto;">NUESTROS CLIENTES</strong></h2>
</div>
<div class="container d-flex justify-content-center">
  <img class="pr-5" src="images/adventista.png" style="max-width: 20%">  
  <img class="pr-5" src="images/adventista.png" style="max-width: 20%">
  <img class="pr-5" src="images/adventista.png" style="max-width: 20%">
  <img class="pr-5" src="images/adventista.png" style="max-width: 20%">
</div>


<section class="section section-text-light section-background section-center section-overlay-opacity section-overlay-opacity-gradient" style="background-image: url(img/custom-header-bg.jpg);">
    <div class="container d-flex justify-content-between" style="margin-left:0px;margin-right:0px;">
        <div class="col-sm-6">
            
                <h4 class="mb-0"><strong class="font-weight-extra-bold" style="font-size:300%">CONTÁCTANOS<br><br>Y CONVÉNCETE</strong></h4>
                <button class="btn btn-primary">contactanos</button>
            
        </div>
        <div class="col-sm-6">
           
                <img src="images/dispositivos.png" style="max-width:100%">
            
        </div>
    </div>
</section>

<div class="container pt-5 text-center">
<h2 class="font-weight-normal line-height-1"><strong class="font-weight-extra-bold" style="font-size:200%">SOLICITA TU PRUEBA</strong></h2>
</div>
<!--Formulario de Contacto-->
                <section id="services" class="section section-height-3 border-0 m-0 appear-animation animated appear-animation-visible" data-appear-animation="fadeIn" style="background-image:url(images/fondo_contacto.png);">
                    <div class="container-fluid" style="margin-top:-3%" style="max-height: 500px;">
                        <div class="row">
                           
                            <div class="col-md-6">
                                
                                <!-- Google Maps - Settings on footer -->
                                
                                    <img src="images/contacto.jpg" style="max-height:100%;width:100%">
                
                            </div>
                            <div class="col-md-6 bg-color-light p-2">
                                
                                    <!-- <div class="featured-box featured-box-primary text-left mt-0"> -->
                                        <div class="box-content">
                                            <form id="contactForm" class="contact-form form-style-2" action="php/contact-form.php" method="POST" novalidate="novalidate">
                                        <div class="contact-form-success alert alert-success d-none mt-4" id="contactSuccess">
                                            <strong>Enviado!</strong> Su mensaje a sido enviado correctamtne.
                                        </div>
                
                                        <div class="contact-form-error alert alert-danger d-none mt-4" id="contactError">
                                            <strong>Error!</strong> Ocurrio un error al enviar su mensaje.
                                            <span class="mail-error-message text-1 d-block" id="mailErrorMessage"></span>
                                        </div>
                                        
                                        <div class="form-row">
                                            <div class="form-group col-xl-6">
                                                <!-- <label for="name" style="font-weight: bold;color:#000000">Nombre</label> -->
                                                <input style="border: 1px solid #8c8c8c;" type="text" value="" data-msg-required="Porfavir ingrese su Nombre" maxlength="100" class="form-control" placeholder="nombre" name="name" id="name" required="">
                                            </div> 
                                            <div class="form-group col-xl-6">
                                                <!-- <label for="email" style="font-weight: bold;color:#000000">Email</label> -->
                                                <input style="border: 1px solid #8c8c8c;" type="email" value="" data-msg-required="Porfavor ingresa tu Email" data-msg-email="Porfavor ingrese un Email valido" maxlength="100" class="form-control" placeholder="email" name="email" id="email" required="">
                                            </div>
                                            <div class="form-group col-xl-6">
                                               <!--  <label for="telefono" style="font-weight: bold;color:#000000">Telefono</label> -->
                                                <input style="border: 1px solid #8c8c8c;" type="text" value="" data-msg-required="Porfavor ingrese su no. Telefonico" 
                                                maxlength="100" class="form-control" placeholder="telefono" name="telefono" id="telefono" required="">
                                            </div>
                                            <div class="form-group col-xl-6">
                                                <!-- <label for="name" style="font-weight: bold;color:#000000">Nombre</label> -->
                                                <input style="border: 1px solid #8c8c8c;" type="text" value="" data-msg-required="Porfavor ingrese el asunto" maxlength="500" class="form-control" placeholder="asunto" name="asunto" id="asunto" required="">
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col">
                                                <!-- <label for="email" style="font-weight: bold;color:#000000">Mensaje</label> -->
                                                <textarea style="border: 1px solid #8c8c8c;" maxlength="5000" data-msg-required="Porfavor ingrese su mensaje." rows="4" class="form-control" placeholder="mensaje" name="message" id="message" required=""></textarea>
                                            </div>
                                        </div>
                                        <div class="form-row d-flex justify-content-center">
                                            
                                            <div class="form-group col"  style="display: flex;justify-content: center;">
                                                <input type="submit" value="SOLICITAR INFORMACIÓN" class="btn font-weight-semibold px-5 btn-py-2 text-2" style="background-color: #4570B3;color:white" data-loading-text="Cargando...">
                                            </div>
                                        </div>
                                    </form>
                                        </div>
                                    <!-- </div> -->
                                
                                </div>
                
                            </div>
                        </div>

                    </div>
                </section>

</div>

<footer id="footer" class="mt-0">
<div class="footer-copyright">
<div class="container py-2">
<div class="row py-4">
<div class="col text-center">
    <ul class="footer-social-icons social-icons social-icons-clean social-icons-icon-light mb-3">
        <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
        <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
        <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
    </ul>
    <p><strong>PORTO TEMPLATE</strong> - © Copyright 2019. All Rights Reserved.</p>
</div>
</div>
</div>
</div>
</footer>
</div>

<!-- Vendor -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="vendor/jquery.cookie/jquery.cookie.min.js"></script>
<script src="vendor/popper/umd/popper.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/common/common.min.js"></script>
<script src="vendor/jquery.validation/jquery.validate.min.js"></script>
<script src="vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="vendor/isotope/jquery.isotope.min.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="vendor/vide/jquery.vide.min.js"></script>
<script src="vendor/vivus/vivus.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="js/theme.js"></script>

<!-- Current Page Vendor and Views -->
<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Current Page Vendor and Views -->
<script src="js/views/view.contact.js"></script>

<!-- Theme Custom -->
<script src="js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="js/theme.init.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY"></script>
<script>

/*
Map Settings

Find the Latitude and Longitude of your address:
- https://www.latlong.net/
- http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude/

*/

// Map Markers
var mapMarkers = [{
address: "New York, NY 10017",
html: "<strong>New York Office</strong><br>New York, NY 10017",
icon: {
image: "img/pin.png",
iconsize: [26, 46],
iconanchor: [12, 46]
},
popup: true
}];

// Map Initial Location
var initLatitude = 40.75198;
var initLongitude = -73.96978;

// Map Extended Settings
var mapSettings = {
controls: {
draggable: (($.browser.mobile) ? false : true),
panControl: true,
zoomControl: true,
mapTypeControl: true,
scaleControl: true,
streetViewControl: true,
overviewMapControl: true
},
scrollwheel: false,
markers: mapMarkers,
latitude: initLatitude,
longitude: initLongitude,
zoom: 16
};

var map = $('#googlemaps').gMap(mapSettings),
mapRef = $('#googlemaps').data('gMap.reference');

// Styles from https://snazzymaps.com/
var styles = [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}];

var styledMap = new google.maps.StyledMapType(styles, {
name: 'Styled Map'
});

mapRef.mapTypes.set('map_style', styledMap);
mapRef.setMapTypeId('map_style');

</script>

<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-12345678-1', 'auto');
ga('send', 'pageview');
</script>
-->

</body>
</html>
